<?php

namespace app\components;

use yii\base\Component;
use WebSocket\Client;
use WebSocket\BadOpcodeException;
use Exception;

/**
 * Class WebsocketComponent
 * @package app\components
 */
class WebsocketComponent extends Component
{
    /** @var Client */
    private $client;
    private $connectionString;

    public $transport;
    public $host;
    public $port;

    /**
     * WebsocketComponent constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * Init
     */
    public function init()
    {
        $this->connectionString = $this->getConnectionString();
        $this->client = new Client($this->connectionString);

        parent::init();
    }

    /**
     * @param $message
     * @return Exception|void|BadOpcodeException
     */
    public function send($message)
    {
        try {
            return $this->client->send($message);
        } catch (BadOpcodeException $e) {
            return $e;
        }
    }

    /**
     * Close websocket client
     */
    public function close()
    {
        $this->client->close();
    }

    /**
     * @return string
     */
    protected function getConnectionString()
    {
        return "{$this->transport}://{$this->host}:{$this->port}";
    }

    /**
     * @return Client
     */
    protected function getClientInstance()
    {
        return $this->client;
    }
}
