<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\base\Exception;
use yii\base\InvalidRouteException;

/**
 * Class MainController
 * @package app\commands
 */
class AppController extends Controller
{
    /**
     * Init app
     *
     * @throws Exception
     * @throws InvalidRouteException
     * @throws \yii\console\Exception
     */
    public function actionInit()
    {
        $this->stdout("Yii2 application initialisation..." . PHP_EOL . PHP_EOL, Console::FG_YELLOW);

        Yii::$app->runAction('migrate/up', ['interactive' => false]);
        echo PHP_EOL;

        $this->stdout("Yii2 application initialisation complete." . PHP_EOL, Console::FG_GREEN);
    }

    /**
     * Down all migrations
     *
     * @throws InvalidRouteException
     * @throws \yii\console\Exception
     */
    public function actionRevert()
    {
        Yii::$app->runAction('migrate/down', ['all', 'interactive' => false]);
    }

    /**
     * Redo application database
     *
     * @throws Exception
     * @throws InvalidRouteException
     * @throws \yii\console\Exception
     */
    public function actionRedo()
    {
        $this->actionRevert();
        $this->actionInit();
    }
}
