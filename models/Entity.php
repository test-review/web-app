<?php

namespace app\models;

/**
 * This is the model class for table "entity".
 *
 * @property string $id
 * @property float|null $param_1
 * @property float|null $param_2
 * @property float|null $param_3
 * @property float|null $param_4
 * @property float|null $param_5
 * @property float|null $param_6
 * @property float|null $param_7
 * @property float|null $param_8
 * @property float|null $param_9
 * @property float|null $param_10
 * @property float|null $param_11
 * @property float|null $param_12
 * @property float|null $param_13
 * @property float|null $param_14
 * @property float|null $param_15
 * @property float|null $param_16
 * @property float|null $param_17
 * @property float|null $param_18
 * @property float|null $param_19
 * @property float|null $param_20
 * @property string|null $updated_at
 */
class Entity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['param_1', 'param_2', 'param_3', 'param_4', 'param_5', 'param_6', 'param_7', 'param_8', 'param_9', 'param_10', 'param_11', 'param_12', 'param_13', 'param_14', 'param_15', 'param_16', 'param_17', 'param_18', 'param_19', 'param_20'], 'number'],
            [['updated_at'], 'safe'],
            [['id'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'param_1' => 'Param 1',
            'param_2' => 'Param 2',
            'param_3' => 'Param 3',
            'param_4' => 'Param 4',
            'param_5' => 'Param 5',
            'param_6' => 'Param 6',
            'param_7' => 'Param 7',
            'param_8' => 'Param 8',
            'param_9' => 'Param 9',
            'param_10' => 'Param 10',
            'param_11' => 'Param 11',
            'param_12' => 'Param 12',
            'param_13' => 'Param 13',
            'param_14' => 'Param 14',
            'param_15' => 'Param 15',
            'param_16' => 'Param 16',
            'param_17' => 'Param 17',
            'param_18' => 'Param 18',
            'param_19' => 'Param 19',
            'param_20' => 'Param 20',
            'updated_at' => 'Updated At',
        ];
    }
}
