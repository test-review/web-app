/**
 * @param color
 * @returns {string}
 */
function contrast(color) {
    let rgb = color.replace(/\s/g, '').match(/^rgba?\((\d+),(\d+),(\d+),?([^,\s)]+)?/i);
    // calculate contrast of color (standard grayscale algorithmic formula)
    return ((Math.round(rgb[1] * 299) + Math.round(rgb[2] * 587) + Math.round(rgb[3] * 114)) / 1000) >= 128 ? 'black' : 'white';
}

/**
 * @param color
 * @param bg_r
 * @param bg_g
 * @param bg_b
 * @returns {string}
 */
function rgba2rgb(color, bg_r, bg_g, bg_b) {
    let rgb = color.replace(/\s/g, '').match(/^rgba?\((\d+),(\d+),(\d+),?([^,\s)]+)?/i),
        alpha = (rgb && rgb[4] || "").trim();
    let r = Math.round(((1 - alpha) * bg_r) + (alpha * rgb[1]));
    let g = Math.round(((1 - alpha) * bg_g) + (alpha * rgb[2]));
    let b = Math.round(((1 - alpha) * bg_b) + (alpha * rgb[3]));
    return `rgb(${r},${g},${b})`;
}

/**
 * @param selectContainer
 * @param attribute
 * @returns {string}
 */
function calcTotal(selectContainer, attribute) {
    const type = selectContainer.options[selectContainer.selectedIndex].text;
    const params = document.querySelectorAll(`.tkbip-table > tbody [data-attribute=${attribute}]`);
    let result = undefined;
    switch (type) {
        case "sum":
            result = 0;
            for (const param of params) {
                result += param.innerHTML * 1;
            }
            break;
        case "min":
            for (const param of params) {
                result = result === undefined ? param.innerHTML * 1 : result > param.innerHTML * 1 ? param.innerHTML * 1 : result;
            }
            break;
        case "max":
            for (const param of params) {
                result = result === undefined ? param.innerHTML * 1 : result < param.innerHTML * 1 ? param.innerHTML * 1 : result;
            }
            break;
        case "avg":
            result = 0;
            for (const param of params) {
                result += param.innerHTML * 1;
            }
            result = result / params.length;
            break;
    }
    return result !== undefined ? result.toFixed(4) : '';
}

// add onchange event listeners
const footers = document.querySelectorAll(`.tkbip-table > tfoot [data-attribute]`);
for (const footer of footers) {
    footer.querySelector(`select`).addEventListener('change', (event) => {
        event.target.nextSibling.innerHTML = calcTotal(event.target, footer.dataset.attribute);
    });
}

// Websocket connection
let ws = new WebSocket('ws://tkbip.test:8081');
ws.onopen = function () {
    console.info(`${Date()} > Connection established!`);
};
ws.onclose = function (event) {
    event.wasClean ? console.info(`${Date()} > Connection closed clean.`) : console.warn(`${Date()} > Connection hang up! Code: ${event.code}` + (event.reason ? `; reason: ${event.reason}` : ``));
};
ws.onmessage = function (event) {
    const data = JSON.parse(event.data);
    const entity = document.querySelector(`[data-key=${data.id}]`);
    if (entity) {
        for (const attribute in data) {
            if (data.hasOwnProperty(attribute) && attribute !== "id") {
                const param = entity.querySelector(`[data-attribute=${attribute}]`);
                param.innerHTML = data[attribute];

                const color = data[attribute] > 0
                    ? `rgba(0,0,0,${Math.abs(data[attribute])});`
                    : data[attribute] < 0
                        ? `rgba(255,140,0,${Math.abs(data[attribute])});`
                        : `rgb(255,255,255);`;

                param.style = `background-color: ${color}; color: ${contrast(rgba2rgb(color, 255, 255, 255))}`;
                document.querySelector(`.tkbip-table > tfoot [data-attribute=${attribute}] select`).dispatchEvent(new Event("change"));
            }
        }
    }
};
ws.onerror = function (error) {
    console.error(`${Date()} > Error: ${error.message}`);
};
