<?php
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

return [
    'bootstrap' => [
        'log'
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'cache' => [
            'class' => \yii\caching\FileCache::class,
        ],
        'db' => $db,
        'websocket' => [
            'class' => \app\components\WebsocketComponent::class,
            'transport' => 'ws',
            'host' => '127.0.0.1',
            'port' => 8010
        ]
    ],
    'params' => $params,
];
