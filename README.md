Тестовое задание для TKBIP на базе шаблона Yii 2 Basic Project Template
------------

[![Latest Stable Version](https://img.shields.io/packagist/v/yiisoft/yii2-app-basic.svg)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Total Downloads](https://img.shields.io/packagist/dt/yiisoft/yii2-app-basic.svg)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![ci-linux](https://github.com/yiisoft/yii2-app-basic/workflows/ci-linux/badge.svg)](https://github.com/yiisoft/yii2-app-basic/actions?query=workflow%3Aci-linux)
[![ci-windows](https://github.com/yiisoft/yii2-app-basic/workflows/ci-windows/badge.svg)](https://github.com/yiisoft/yii2-app-basic/actions?query=workflow%3Aci-windows)

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

- [VirtualBox](https://www.virtualbox.org/)
- [Vagrant](https://www.vagrantup.com/downloads.html)

INSTALLATION
------------

Из рут диретории проекта выполнить команду `vagrant up` при этом будет установлена и развёрнута виртуальная машина, с настроенным ПО.
Доступ к машине воможно получить выполнив команду `vagrant ssh` и далее работать как с обычным linux Debian ветки.
После установки будет доступна страница с адресом `http://tkbip.test`

CONFIGURATION
-------------

По заданию все сервисы создаются/запускаются на выбор разработчика:

- `websocket service`: в проекте реализован простейший вебсокет сервер, задача отдавать по протоколу WS данные всем подключенным клиентам. Стоит за прокси сервером Nginx. Отдан под управление systemD.
      - `sudo service websocket restart `
- `data-source service`: это `PUB` реализация классичечкого паттерна `PUB/SUB`, отдан под управление systemD, запуск/остановка/перезапуск выполняются соотвествующими командами на виртуальной машине:
      - `sudo service data-source status`
      - `sudo service data-source stop`
      - `sudo service data-source restart`
- `business service`: это реализация `SUB` паттерна `PUB/SUB`, так же отдан под управление systemD, взаимодействуюет с (`websocket server`), а так же пишет данные в базу данных (`mariadb 10.4`)   
      - `sudo service business restart`

Для всех сервисов есть свои [репозитории](https://bitbucket.org/test-review/workspace/projects/TKBIP), вся остальная конфигурация относящаяся к веб приложению на базе шаблона [Yii 2 Basic Project Template](https://github.com/yiisoft/yii2-app-basic#configuration) 
    
TESTING
-------

Tests are located in `tests` directory. They are developed with [Codeception PHP Testing Framework](http://codeception.com/).
By default there are 3 test suites:

- `unit`
- `functional`
- `acceptance`

Tests can be executed by running

```
vendor/bin/codecept run
```

The command above will execute unit and functional tests. Unit tests are testing the system components, while functional
tests are for testing user interaction. Acceptance tests are disabled by default as they require additional setup since
they perform testing in real browser. 


### Running  acceptance tests

To execute acceptance tests do the following:  

1. Rename `tests/acceptance.suite.yml.example` to `tests/acceptance.suite.yml` to enable suite configuration

2. Replace `codeception/base` package in `composer.json` with `codeception/codeception` to install full featured
   version of Codeception

3. Update dependencies with Composer 

    ```
    composer update  
    ```

4. Download [Selenium Server](http://www.seleniumhq.org/download/) and launch it:

    ```
    java -jar ~/selenium-server-standalone-x.xx.x.jar
    ```

    In case of using Selenium Server 3.0 with Firefox browser since v48 or Google Chrome since v53 you must download [GeckoDriver](https://github.com/mozilla/geckodriver/releases) or [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads) and launch Selenium with it:

    ```
    # for Firefox
    java -jar -Dwebdriver.gecko.driver=~/geckodriver ~/selenium-server-standalone-3.xx.x.jar
    
    # for Google Chrome
    java -jar -Dwebdriver.chrome.driver=~/chromedriver ~/selenium-server-standalone-3.xx.x.jar
    ``` 
    
    As an alternative way you can use already configured Docker container with older versions of Selenium and Firefox:
    
    ```
    docker run --net=host selenium/standalone-firefox:2.53.0
    ```

5. (Optional) Create `yii2basic_test` database and update it by applying migrations if you have them.

   ```
   tests/bin/yii migrate
   ```

   The database configuration can be found at `config/test_db.php`.


6. Start web server:

    ```
    tests/bin/yii serve
    ```

7. Now you can run all available tests

   ```
   # run all available tests
   vendor/bin/codecept run

   # run acceptance tests
   vendor/bin/codecept run acceptance

   # run only unit and functional tests
   vendor/bin/codecept run unit,functional
   ```

### Code coverage support

By default, code coverage is disabled in `codeception.yml` configuration file, you should uncomment needed rows to be able
to collect code coverage. You can run your tests and collect coverage with the following command:

```
#collect coverage for all tests
vendor/bin/codecept run --coverage --coverage-html --coverage-xml

#collect coverage only for unit tests
vendor/bin/codecept run unit --coverage --coverage-html --coverage-xml

#collect coverage for unit and functional tests
vendor/bin/codecept run functional,unit --coverage --coverage-html --coverage-xml
```

You can see code coverage output under the `tests/_output` directory.
