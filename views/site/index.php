<?php
/* @var $this yii\web\View */

/* @var $searchModel EntitySearch */

/* @var $dataProvider ActiveDataProvider */

use app\models\EntitySearch;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

$this->title = 'My Yii Application';

$totalLabels = ['sum', 'min', 'max', 'avg'];

function getSum($provider, $attribute)
{
    $sum = 0;
    foreach ($provider as $item) {
        $sum += $attribute !== "id" && $attribute !== "updated_at" ? $item->$attribute : 0;
    }
    return Yii::$app->formatter->asDecimal($sum, 4);
}

$columns = [];
foreach ($searchModel->attributeLabels() as $key => $name) {
    $columns[$key] = [
        'attribute' => $key,
        'format' => 'raw',
        'contentOptions' => function () use ($key) {
            return [
                'data-attribute' => $key
            ];
        },
        'footer' => Html::tag('div', Html::dropDownList('name', 0, $totalLabels, [
                'style' => 'width:100%'
            ]) . Html::tag('div', getSum($dataProvider->getModels(), $key), [
                'style' => 'margin:8px 0 0 2px'
            ]), ['data-attribute' => $key])
    ];
}

$columns['id']['footer'] = 'ИТОГО:';
$columns['id']['value'] = function ($searchModel) {
    return Html::tag('a', $searchModel->id, ['href' => Url::toRoute(['index', 'EntitySearch' => ['id' => $searchModel->id]])]);
};

unset($columns['updated_at']);

?>
<div class="site-index">
    <br/>
    <br/>
    <br/>
    <div class="body-content">
        <?php try {
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'summary' => false,
                'tableOptions' => ['class' => 'table table-bordered tkbip-table'], //table tag
                'columns' => $columns,
                'showFooter' => true
            ]);
        } catch (Exception $e) {
            echo $e->getMessage();
        } ?>
    </div>
</div>
