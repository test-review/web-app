<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%entity}}`.
 */
class m200917_204819_create_entity_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $params = [
            'id' => $this->string()->unique()->notNull()
        ];
        for ($i = 0; $i < 20; $i++) {
            $params["param_".($i+1)] = $this->double()->null();
        }
        $params['updated_at'] = $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP');

        $this->createTable('{{%entity}}', $params);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%entity}}');
    }
}
