#!/usr/bin/env bash

#== Import script args ==

timezone=$(echo "$1")

#== Bash helpers ==

function info {
  echo " "
  echo "--> $1"
  echo " "
}

#== Provision script ==

info "Provision-script user: `whoami`"

export DEBIAN_FRONTEND=noninteractive

info "Configure timezone"
timedatectl set-timezone ${timezone} --no-ask-password

info "Prepare root password for MySQL > root/[empty]"
debconf-set-selections <<< "mariadb-server-10.4 mysql-server/root_password password \"''\""
debconf-set-selections <<< "mariadb-server-10.4 mysql-server/root_password_again password \"''\""
echo "Done!"

apt-get install -y software-properties-common dirmngr
apt-key adv --fetch-keys 'https://mariadb.org/mariadb_release_signing_key.asc'
add-apt-repository 'deb [arch=amd64] http://mirror.truenetwork.ru/mariadb/repo/10.4/debian buster main'

apt install -y curl wget gnupg2 ca-certificates lsb-release apt-transport-https
wget https://packages.sury.org/php/apt.gpg
apt-key add apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php7.list

info "Update OS software"
apt-get update
apt-get upgrade -y

info "Install additional software"
apt-get install -y git htop mc php7.2-curl php7.2-cli php7.2-intl php7.2-mysqlnd php7.2-gd php7.2-fpm php7.2-mbstring php7.2-xml unzip nginx mariadb-server-10.4 php.xdebug net-tools

info "Install NodeJs"
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
apt-get install -y nodejs

info "Configure MySQL"
sed -i "s/.*bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf
sed -i "s/#default-character-set.*/default-character-set = utf8mb4/" /etc/mysql/mariadb.cnf
sed -i "s/#character-set-server.*/character-set-server = utf8mb4/" /etc/mysql/mariadb.cnf
sed -i "s/#collation-server.*/collation-server = utf8mb4_unicode_ci/" /etc/mysql/mariadb.cnf
sed -i "s/#character_set_server.*/character_set_server = utf8mb4/" /etc/mysql/mariadb.cnf
sed -i "s/#collation_server.*/collation_server = utf8mb4_unicode_ci/" /etc/mysql/mariadb.cnf
mysql -uroot <<< "CREATE USER 'root'@'%' IDENTIFIED BY ''"
mysql -uroot <<< "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%'"
mysql -uroot <<< "DROP USER 'root'@'localhost'"
mysql -uroot <<< "FLUSH PRIVILEGES"
echo "Done!"

info "Restart MySQL service"
service mysql restart
echo "Done!"

info "Configure PHP-FPM"
sed -i 's/user = www-data/user = vagrant/g' /etc/php/7.2/fpm/pool.d/www.conf
sed -i 's/group = www-data/group = vagrant/g' /etc/php/7.2/fpm/pool.d/www.conf
sed -i 's/owner = www-data/owner = vagrant/g' /etc/php/7.2/fpm/pool.d/www.conf
cat << EOF > /etc/php/7.2/mods-available/xdebug.ini
zend_extension=xdebug.so
xdebug.remote_enable=1
xdebug.remote_connect_back=1
xdebug.remote_port=9000
xdebug.remote_autostart=1
EOF
echo "Done!"

info "Configure NGINX"
sed -i 's/user www-data/user vagrant/g' /etc/nginx/nginx.conf
echo "Done!"

info "Enabling site configuration"
ln -s /app/vagrant/nginx/app.conf /etc/nginx/sites-enabled/app.conf
echo "Done!"

info "Initialize databases for MySQL"
mysql -uroot <<< 'CREATE DATABASE `tkbip`'
mysql -uroot <<< 'CREATE DATABASE `tkbip_test`'
echo "Done!"

info "Setup php defaults to 7.2"
update-alternatives --set php /usr/bin/php7.2

info "Install composer"
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
