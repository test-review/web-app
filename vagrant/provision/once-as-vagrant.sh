#!/usr/bin/env bash

#== Import script args ==

github_token=$(echo "$1")

#== Bash helpers ==

function info {
  echo " "
  echo "--> $1"
  echo " "
}

#== Provision script ==

info "Provision-script user: `whoami`"

info "Clone Repositories"
git clone https://mat-twg@bitbucket.org/test-review/websocket.git ~/websocket
cd ~/websocket && npm install
sudo ln -s /home/vagrant/websocket/websocket.service /etc/systemd/system/websocket.service
sudo systemctl daemon-reload
sudo systemctl enable websocket

git clone https://mat-twg@bitbucket.org/test-review/data-source.git ~/data-source
cd ~/data-source && npm install
node init
cp config.temp.json config.json
sudo ln -s /home/vagrant/data-source/data-source.service /etc/systemd/system/data-source.service
sudo systemctl daemon-reload
sudo systemctl enable data-source

git clone https://mat-twg@bitbucket.org/test-review/business-service.git ~/business-service
cd ~/business-service && npm install
node init
cp config.temp.json config.json
cp ~/data-source/.keys/public_key.curve ~/business-service/.keys/server_public_key.curve
sudo ln -s /home/vagrant/business-service/business.service /etc/systemd/system/business.service
sudo systemctl daemon-reload
sudo systemctl enable business

info "Configure composer"
composer config --global github-oauth.github.com ${github_token}
echo "Done!"

info "Install project dependencies"
cd /app
composer --no-progress --prefer-dist install

info "Create bash-alias 'app' for vagrant user"
echo 'alias app="cd /app"' | tee /home/vagrant/.bash_aliases

info "Enabling colorized prompt for guest console"
sed -i "s/#force_color_prompt=yes/force_color_prompt=yes/" /home/vagrant/.bashrc

php yii app/init
